-- CRIANDO TABELA students
DROP TABLE IF EXISTS students;
CREATE TABLE students
(
    id BIGINT,
    student VARCHAR
);

-- INSERINDO registros
INSERT INTO students
VALUES
    (1, 'Abbot'),
    (2, 'Doris'),
    (3, 'Emerson'),
    (4, 'Green'),
    (5, 'Jeames');

-- Se ID for par, este será lagado, se ímpar, será atribuído o próximo ID
SELECT 
  id, 
  COALESCE(
    CASE WHEN t.rn % 2 = 0 THEN LAG(student) OVER (
      ORDER BY 
        id
    ) ELSE LEAD(student) OVER (
      ORDER BY 
        id
    ) END, 
    student
  ) AS student 
FROM 
  (
    SELECT 
      id, 
      student, 
      ROW_NUMBER() OVER (
        ORDER BY 
          id
      ) AS rn 
    FROM 
      students
  ) t