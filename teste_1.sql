-- GROUP BY de Total de "ads" por dia, por pais. (A) (Q1)
WITH total_ads AS (
    SELECT
            data_nk
            ,regiao_nk
            ,pais_nk
            ,COUNT (pais_nk) AS Q1
        FROM
            ads_staging
        GROUP BY
            data_nk
            ,regiao_nk
            ,pais_nk
        ORDER BY
            data_nk
            ,regiao_nk
            ,pais_nk
)
-- GROUP BY de Total de "ads" na data anterior por pais. (B) (Q2)
,lag_total_ads AS (
    SELECT
            data_nk
            ,regiao_nk
            ,pais_nk
            ,Q1
            ,LAG (
                Q1
                ,1
            ) OVER (
                PARTITION BY pais_nk
            ORDER BY
                data_nk
            ) Q2
        FROM
            total_ads
        ORDER BY
            data_nk
            ,regiao_nk
            ,pais_nk
)
-- CRIANDO COLUNA com a diferença entra (A) e (B). (Q3)
,diff_total_ads AS (
    SELECT
            data_nk
            ,regiao_nk
            ,pais_nk
            ,q1
            ,q2
            ,(
                q1 - q2
            ) AS Q3
        FROM
            lag_total_ads
)
-- GROUP BY de Total de "ads" por dia, por região. (Q4)
,region_total_ads AS (
    SELECT
            data_nk
            ,regiao_nk
            ,COUNT (regiao_nk) AS Q4
        FROM
            ads_staging
        GROUP BY
            data_nk
            ,regiao_nk
        ORDER BY
            data_nk
            ,regiao_nk
)
-- CRIANDO COLUNA com o percentual de "ads" por pais/data dentro de sua região. (Q5)
,region_diff_total_ads AS (
    SELECT
            diff_total_ads.data_nk
            ,diff_total_ads.regiao_nk
            ,diff_total_ads.pais_nk
            ,diff_total_ads.Q1
            ,diff_total_ads.Q2
            ,diff_total_ads.Q3
            ,region_total_ads.Q4
        FROM
            diff_total_ads LEFT JOIN region_total_ads
                ON diff_total_ads.data_nk = region_total_ads.data_nk
            AND diff_total_ads.regiao_nk = region_total_ads.regiao_nk
) SELECT
        data_nk
        ,regiao_nk
        ,pais_nk
        ,Q1
        ,Q2
        ,Q3
        ,Q4
        ,ROUND( ( Q1::DECIMAL / Q4::DECIMAL ) * 100 ,2 ) AS Q5
    FROM
        region_diff_total_ads