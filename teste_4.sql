-- CRIANDO TABELA departament
DROP 
  TABLE IF EXISTS departament CASCADE;
CREATE TABLE departament (Id INT PRIMARY KEY, Name VARCHAR);
-- INSERINDO valores
INSERT INTO departament 
VALUES 
  (1, 'IT'), 
  (2, 'Sales');
-- CRIANDO TABELA employee
DROP 
  TABLE IF EXISTS employee;
CREATE TABLE employee (
  Id INT PRIMARY KEY, 
  Name VARCHAR, 
  Salary NUMERIC, 
  DepartmentId INT references departament(id)
);
-- INSERINDO valores
INSERT INTO employee 
VALUES 
  (1, 'Joe', 70000, 1), 
  (2, 'Jim', 90000, 1), 
  (3, 'Henry', 80000, 2), 
  (4, 'Sam', 60000, 2), 
  (5, 'Max', 90000, 1);
-- LEFT JOIN employee_departament
WITH employee_departament AS (
  SELECT 
    departament.name AS departament, 
    employee.name AS employee, 
    employee.salary 
  FROM 
    employee 
    LEFT JOIN departament ON employee.departmentid = departament.id
),
-- RANKEANDO os salários por departamento e selecionado salários com rank 1 
cte AS (
  SELECT 
    departament, 
    employee, 
    salary, 
    RANK() OVER (
      PARTITION BY departament 
      ORDER BY 
        salary DESC
    ) AS r 
  FROM 
    employee_departament
) 
SELECT 
  departament, 
  employee, 
  salary 
FROM 
  cte 
WHERE 
  r = 1 
ORDER BY 
  departament ASC;
